/*
 * Copyright 2007-2013 Peter Doornbosch
 *
 * This file is part of TopThreads, a JConsole plugin to analyse CPU-usage per thread.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * TopThreads is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package net.luminis.jmx.sample;

public class MultiThreadSample {

    static int sleep;
    static int loop;

    /**
     * @param args
     */
    public static void main(String[] args) {
        int num = 1;
        if (args.length == 0) {
            sleep = 1000;
            System.err.println("missing argument (int: number of threads), defaulting to " + num + " and sleep = " + sleep);
        }
        else {
            try {
                num = Integer.parseInt(args[0]);
                sleep = Integer.parseInt(args[1]);
                loop = Integer.parseInt(args[2]);
                System.out.println(String.format("#threads: %d, sleep: %d, loop: %d", num, sleep, loop));
            }
            catch (NumberFormatException nfe) {
                System.err.println("wrong argument: not a number");
            }
        }

        for (int i = 0; i < num; i++) {
            String name = "thread" + (i+1);
            Thread t = new Thread(new WorkerThread(), name);
            int prio = i + 1;
            if (prio <= Thread.MAX_PRIORITY && prio >= Thread.MIN_PRIORITY)
                t.setPriority(prio);
            t.start();
        }
    }

    public static class WorkerThread implements Runnable
    {
        public void run() {
            int number = 9;
            double result;
            while (true) {
                for (int i = 0; i < loop; i++)
                    result = Math.pow(number++, 2);
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                }
            }
        }
    }

}
