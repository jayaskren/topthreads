/*
 * Copyright 2007-2013 Peter Doornbosch
 *
 * This file is part of TopThreads, a JConsole plugin to analyse CPU-usage per thread.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * TopThreads is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package net.luminis.jmx.topthreads;

import junit.framework.TestCase;

public class ThreadInfoStatsTest extends TestCase
{
    private ThreadInfoStats threadInfoStats0;
    private ThreadInfoStats threadInfoStats100;

    @Override
    public void setUp()
    {
        long initial = 0;
        threadInfoStats0 = new ThreadInfoStats(0, null, initial * 1000);

        initial = 100;
        threadInfoStats100 = new ThreadInfoStats(0, null, initial * 1000);
    }

    public void testInitial()
    {
        threadInfoStats0.update(null, 60 * 1000);
        threadInfoStats0.computePercentage(300 * 1000);

        // percentage returns the actual
        assertEquals(20, threadInfoStats0.getPercentage());
        int[] history = threadInfoStats0.getHistory();
        // and yet, there is not history
        assertEquals(0, history[0]);
    }

    public void testSecondPeriod()
    {
        // period 1
        threadInfoStats0.update(null, 60 * 1000);
        threadInfoStats0.computePercentage(300 * 1000);
        // period 2
        threadInfoStats0.update(null, (60 + 120) * 1000);
        threadInfoStats0.computePercentage(300 * 1000);

        // percentage returns the actual
        assertEquals(40, threadInfoStats0.getPercentage());
        // average is now
        assertEquals(30, threadInfoStats0.getAverageUsage());
        int[] history = threadInfoStats0.getHistory();
        // and yet, there is history
        assertEquals(20, history[history.length-1]);
    }

}
