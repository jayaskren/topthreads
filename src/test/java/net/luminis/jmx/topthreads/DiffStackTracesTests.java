/*
 * Copyright 2007-2013 Peter Doornbosch
 *
 * This file is part of TopThreads, a JConsole plugin to analyse CPU-usage per thread.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * TopThreads is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package net.luminis.jmx.topthreads;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;


public class DiffStackTracesTests extends TestCase {

    public void testMatchComplete() {

        String[] st1 = { "een", "twee", "drie", "vier" };
        String[] st2 = { "een", "twee", "drie", "vier" };

        DiffStackTraces diff = new DiffStackTraces(st1, st2).diff();

        assertEquals(1, diff.getNrOfMatches());
        assertPairEquals(0, 3, diff.getMatchFrom(0));
        assertPairEquals(0, 3, diff.getMatchTo(0));
    }

    public void testSimplestCase() {

        String[] st1 = { "een", "twee", "drie", "vier" };
        String[] st2 = { "aap", "noot", "drie", "vier" };

        DiffStackTraces diff = new DiffStackTraces(st1, st2).diff();

        assertEquals(1, diff.getNrOfMatches());
        assertPairEquals(2, 3, diff.getMatchFrom(0));
        assertPairEquals(2, 3, diff.getMatchTo(0));
    }

    public void testLastRowDoesNotMatch() {

        String[] st1 = { "nul", "een", "twee", "drie", "vier" };
        String[] st2 = { "aap", "noot", "drie", "vier", "huh?" };

        DiffStackTraces diff = new DiffStackTraces(st1, st2).diff();

        assertEquals(1, diff.getNrOfMatches());
        assertPairEquals(3, 4, diff.getMatchFrom(0));
        assertPairEquals(2, 3, diff.getMatchTo(0));
    }

    public void testDoubleMatch() {

        String[] st1 = { "nul", "een", "twee", "drie", "vier" };
        String[] st2 = { "0", "1", "een", "twee", "drie", "5", "drie", "vier", "8" };

        DiffStackTraces diff = new DiffStackTraces(st1, st2).diff();

        assertEquals(2, diff.getNrOfMatches());
        assertPairEquals(3, 4, diff.getMatchFrom(0));
        assertPairEquals(6, 7, diff.getMatchTo(0));
        assertPairEquals(1, 3, diff.getMatchFrom(1));
        assertPairEquals(2, 4, diff.getMatchTo(1));
    }

    public void testDoubleConsequetiveMatch() {

        String[] st1 = { "nul", "een", "twee", "drie", "vier" };
        String[] st2 = { "0", "1", "een", "twee", "drie", "drie", "vier", "8" };

        DiffStackTraces diff = new DiffStackTraces(st1, st2).diff();

        assertEquals(2, diff.getNrOfMatches());
        assertPairEquals(3, 4, diff.getMatchFrom(0));
        assertPairEquals(5, 6, diff.getMatchTo(0));
        assertPairEquals(1, 3, diff.getMatchFrom(1));
        assertPairEquals(2, 4, diff.getMatchTo(1));
    }

    public void testOverlappingMatches() {
                       // 0    1    2    3    4    5    6    7
        String[] st1 = { "l", "e", "b", "b", "a", "b", "a", "b" };
        String[] st2 = { "l", "e", "b", "b", "a", "b" };

        System.out.println("*************************");
        DiffStackTraces diff = new DiffStackTraces(st1, st2).diff();

        assertEquals(2, diff.getNrOfMatches());
        assertPairEquals(5, 7, diff.getMatchFrom(0));
        assertPairEquals(3, 5, diff.getMatchTo(0));
        assertPairEquals(0, 5, diff.getMatchFrom(1));
        assertPairEquals(0, 5, diff.getMatchTo(1));
    }

    public void assertPairEquals(int from, int to, int[] pair) {
        if (from !=  pair[0] || to != pair[1]) {
            throw new AssertionFailedError("Expected [" + from + ":" + to + "], but got [" + pair[0] + ":" + pair[1] + "]");
        }
    }
}