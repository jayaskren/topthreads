/*
 * Copyright 2007-2013 Peter Doornbosch
 *
 * This file is part of TopThreads, a JConsole plugin to analyse CPU-usage per thread.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * TopThreads is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package net.luminis.jmx.topthreads;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

import javax.management.MBeanServerConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.Comparator;

import static java.lang.management.ManagementFactory.*;

public class TextUI
{
    private static String lotsOfSpaces = "                                             ";
    private static final Comparator<InfoStats> lastUsageComparator = ThreadInfoStats.lastUsageComparator();
    private static final Comparator<InfoStats> fixOrderComparator = ThreadInfoStats.fixOrderComparator();

    private final PrintStream out;
    private String processName;
    private int interval = 3;
    private boolean clearOnNextDraw = false;
    private int previousVerticalSize;
    private int phase = 0;
    private final ThreadDataCollector threadDataCollector;
    private volatile boolean showCpuUsageHistory;
    private volatile int maxThreadsDisplayed;
    private volatile boolean showStackTrace;
    private volatile int showStackTraceFor;
    private long secondaryScreenTimeout = 30 * 1000;
    private int maxColumns = 80;
    private volatile int maxRows = 24;
    private volatile Comparator<InfoStats> comparator;
    private String threadColumnHeader;
    private boolean showAbout;
    private boolean showHelp;

    public TextUI(String hostname, int port) {

        maxThreadsDisplayed = 20;
        showCpuUsageHistory = true;
        boolean debug = false;
        comparator = lastUsageComparator;

        ThreadMXBean threadMXBean = null;
        try {
            MBeanServerConnection server = Launcher.connect(hostname, port);
            threadMXBean = newPlatformMXBeanProxy(server, THREAD_MXBEAN_NAME, ThreadMXBean.class);
            RuntimeMXBean runtimeMXBean = newPlatformMXBeanProxy(server, RUNTIME_MXBEAN_NAME, RuntimeMXBean.class);
            processName = runtimeMXBean.getName();
        } catch (IOException e) {
            System.err.println("Could not connect to '" + hostname + ":" + port + "' (" + e + ")");
            System.exit(1);
        }

        threadDataCollector = new ThreadDataCollector(threadMXBean, debug);


        final Thread processThread = new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    Data data = threadDataCollector.getThreadData(maxThreadsDisplayed, showCpuUsageHistory, false, comparator, null, 20);

                    updateUI(data);
                    try {
                        Thread.sleep(interval * 1000);
                    } catch (InterruptedException e) {
                        // Nothing
                    }

                    if (showStackTrace) {
                        int index = showCpuUsageHistory? showStackTraceFor + 1: showStackTraceFor;
                        if (data.threadList.size() > index) {
                            int lines = showStackTrace((ThreadInfoStats) data.threadList.get(index));
                            if (lines == 0) {
                                showStackTrace = false;
                            } else {
                                previousVerticalSize = lines;
                            }
                        }
                        else {
                            System.err.println("no stacktrace to show");
                        }
                    }

                    if (showAbout || showHelp || showStackTrace) {
                        if (showAbout) showAbout();
                        if (showHelp)  showHelp();
                        try {
                            Thread.sleep(secondaryScreenTimeout);
                        } catch (InterruptedException e) {
                            // Nothing
                        }
                        showAbout = showHelp = showStackTrace = false;
                        out.print(Ansi.ansi().eraseScreen());
                    }
                }
            }
        });

        Thread cmdLoopThread = new Thread(new Runnable() {
            @Override
            public void run() {
                commandLoop(processThread);
            }
        });

        AnsiConsole.systemInstall();
        out = AnsiConsole.out();
        out.print(Ansi.ansi().eraseScreen());

        cmdLoopThread.start();
        processThread.start();
        try {
            cmdLoopThread.join();
        } catch (InterruptedException e) {
            // Exit
        }
    }

    private int showStackTrace(ThreadInfoStats threadInfo) {
        StackTraceElement[] stackTraceElements = threadDataCollector.retrieveStackTrace(threadInfo.getId(), maxRows);
        if (stackTraceElements.length > 0) {
            int lineNr = 1;
            out.print(Ansi.ansi().eraseScreen());
            out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine().bold().a("Stacktrace").boldOff().a(" for thread " + threadInfo.getName()));
            for (StackTraceElement stackTraceElement : stackTraceElements) {
                out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine().a(trim("" + (lineNr - 1) + " " + stackTraceElement, maxColumns)));
                if (lineNr > maxRows)
                    break;
            }
            out.flush();
            return lineNr - 1;
        } else {
            return 0;
        }
    }

    void commandLoop(Thread processThread) {
        String input = "";
        try {
            BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
            while ((input = stdin.readLine()) != null) {
                try {
                    if (input.equals("q")) {
                        System.err.println("bye...");
                        System.exit(0);
                    } else if (input.equals("h")) {
                        showHelp = true;
                        processThread.interrupt();
                    } else if (input.equals("r")) {
                        clearOnNextDraw = true;
                        processThread.interrupt();
                    } else if (input.equals("f")) {
                        if (comparator == lastUsageComparator) {
                            comparator = fixOrderComparator;
                        }
                        else {
                            comparator = lastUsageComparator;
                        }
                        processThread.interrupt();
                    } else if (input.startsWith("s")) {
                        showStackTraceFor = 0;
                        if (! input.equals("s")) {
                            showStackTraceFor = Integer.parseInt(input.substring(1).trim())-1;
                        }
                        showStackTrace = true;
                        processThread.interrupt();
                    } else if (input.startsWith("l")) {
                        if (! input.equals("l")) {
                            int value = Integer.parseInt(input.substring(1).trim());
                            if (value >= 7) {
                                maxRows = value;
                            }
                        }
                        processThread.interrupt();
                    } else if (input.startsWith("i")) {
                        int value = Integer.parseInt(input.substring(1));
                        if (value > 0) {
                            interval = value;
                            processThread.interrupt();
                        }
                    } else if (input.equals("a")) {
                        showAbout = true;
                        processThread.interrupt();
                    } else if (input.trim().length() > 0) {
                        System.err.println("unknown input: " + input);
                    } else if (showStackTrace || showAbout || showHelp) {
                        showHelp = showStackTrace = showAbout = false;
                        processThread.interrupt();
                    }
                } catch (NumberFormatException e) {
                    // Don't bother
                }
            }
        }
        catch(IOException e){
            System.err.println("Error in reading commands");
            return;
        }
    }

    void updateUI(Data data) {
        PrintStream out = AnsiConsole.out();

        if (clearOnNextDraw) {
            out.print(Ansi.ansi().eraseScreen());
            clearOnNextDraw = false;
        }

        int lineNr = 1;
        out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine().a("  ").bold().a("TopThreads " + pad("2.0", 5, false)).boldOff()
                .a(" connected to \"").a(pad(processName + "\"", 30, false))
                .a("CPU: ").bold().a(pad(data.cpuUsagePercentage, 3) + "%").boldOff()
        );

        int countPrecision = data.threadCount > 999? 4: data.threadCount > 99? 3: data.threadCount > 9? 2: 1;
        out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine().a(""
                + data.threadCount + " threads  "
                + pad(data.threadStats.get(Thread.State.RUNNABLE),      countPrecision) + " runnable  "
                + pad(data.threadStats.get(Thread.State.BLOCKED),       countPrecision) + " blocked  "
                + pad(data.threadStats.get(Thread.State.WAITING),       countPrecision) + " waiting  "
                + pad(data.threadStats.get(Thread.State.TIMED_WAITING), countPrecision) + " timed-waiting"
        ));
        out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine());
        String arrow = pad(lotsOfSpaces.substring(0, phase + 2) + ">    >    >    >", 20, false);
        phase = (phase + 1) % 5;
        threadColumnHeader = "THREAD NAME " + (comparator == fixOrderComparator? " (fixed order)": "");
        out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine().a(pad(threadColumnHeader, 40, false) + "CPU" + "  CPU HISTORY" + arrow));

        int index = 1;
        for (InfoStats info: data.threadList) {
            out.print(Ansi.ansi().cursor(lineNr, 0).eraseLine());
            String line = "";
            if (info instanceof ThreadInfoStats)
                line = pad(index++, 2) + " ";
            else
                line = "   ";
            line += pad(info.getName(), 40, false) + " ";
            line += colorize(info.getPercentage(), pad(info.getPercentage(), 2) + " ");
            int[] history = info.getHistory();
            for (int i = history.length-1; i >= 0; i--) {
                line += " " + colorize(history[i], pad(history[i], 2));
            }
            out.println(line);
            lineNr++;
            if (lineNr == maxRows)
                break;
        }
        out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine().a("Period: ").a(pad("" + interval + "s", 5, false))
                .bold().a("Command").boldOff().a(" (+Enter) h=Help: "));
        int currentLines = lineNr - 1;
        while (lineNr <= previousVerticalSize) {
            out.print(Ansi.ansi().cursor(lineNr++, 0).eraseLine());
        }
        out.flush();

        previousVerticalSize = currentLines;
    }

    private String colorize(int threshold, String value) {
        if (threshold < 2) {
            return Ansi.ansi().fg(Ansi.Color.YELLOW).a(value).fg(Ansi.Color.DEFAULT).toString();
        }
        else if (threshold > 90) {
            return Ansi.ansi().fgBright(Ansi.Color.RED).a(value).fg(Ansi.Color.DEFAULT).toString();
        }
        else
            return value;
    }

    private String pad(int number, int width) {
        return pad("" + number, width, true);
    }

    private String pad(String text, int width, boolean right) {
        if (text.length() == width)
            return text;
        if (text.length() >= width)
            return text.substring(0, width);
        int padSize = width - text.length();
        while (lotsOfSpaces.length() < padSize) {
            lotsOfSpaces = lotsOfSpaces + lotsOfSpaces;
        }
        String pad = lotsOfSpaces.substring(0, padSize);
        if (right)
            return pad + text;
        else
            return text + pad;
    }

    private String trim(String text, int length) {
        if (text.length() == length)
            return text;
        if (text.length() >= length)
            return text.substring(0, length);
        return text;
    }

    private void showHelp() {
        out.print(Ansi.ansi().eraseScreen().cursor(0, 0));
        out.println(Ansi.ansi().bold().a("  TopThreads Help").boldOff());
        out.println("");
        out.println(" f          fix thread order (toggle)");
        out.println(" i <number> set refresh interval (in seconds)");
        out.println(" s [number] show stacktrace for thread (default is top thread)");
        out.println(" l <number> number of lines to use for display (must be >= 7)");
        out.println(" r          refresh display");
        out.println(" h          help");
        out.println(" a          about");
        out.println(" q          quit");
        out.println("");
        out.println("(<Enter> to return)");
    }

    private void showAbout() {
        out.print(Ansi.ansi().eraseScreen().cursor(0, 0));
        out.println("");
        out.println("   _____         _____ _                 _        ___   ___ ");
        out.println("  |_   _|___ ___|_   _| |_ ___ ___ ___ _| |___   |_  | |   |");
        out.println("    | | | . | . | | | |   |  _| -_| .'| . |_ -|  |  _|_| | |");
        out.println("    |_| |___|  _| |_| |_|_|_| |___|__,|___|___|  |___|_|___|");
        out.println("            |_|");
        out.println("");
        out.println("");
        out.println("  created by Peter Doornbosch (peter.doornbosch@luminis.eu)");
        out.println("  http://arnhem.luminis.eu/new_version_topthreads_jconsole_plugin/");
        out.println("");
        out.println("");
        out.println("  (<Enter> to return)");
    }
}
