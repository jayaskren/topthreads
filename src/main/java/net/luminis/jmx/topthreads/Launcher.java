/*
 * Copyright 2007-2013 Peter Doornbosch
 *
 * This file is part of TopThreads, a JConsole plugin to analyse CPU-usage per thread.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * TopThreads is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package net.luminis.jmx.topthreads;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class Launcher
{
    public static final String PROGRAM_NAME = "TopThreads";
    public static final String VERSION = "2.0-beta";
    public static final String JARNAME = "topthreads-" + VERSION + ".jar";

    public static final String DEBUG_FLAG = "net.luminis.jmx.topthreads.debug";

    static boolean debug;

    static void usageAndExit() {
        System.out.println("");
        System.out.println("TopThreads version " + VERSION);
        System.out.println("");
        System.out.println("Use as JConsole plugin: jconsole -pluginpath " + JARNAME);
        System.out.println("");
        System.out.println("Stand alone usage: java -jar " + JARNAME + " <hostname>:<port>");
        System.out.println("               or: java -jar " + JARNAME + " --textui <hostname>:<port>");
        System.out.println("");
        System.exit(1);
    }

    public static void main(String[] args) throws Exception {

        if (System.getProperty(DEBUG_FLAG) != null && System.getProperty(DEBUG_FLAG).equals("true"))
            debug = true;

        String hostname = null;
        Integer port = null;

        List<String> options = new ArrayList<String>();
        for (String arg: args) {
            if (arg.startsWith("-")) {
                options.add(arg);
            }
            else {
                if (hostname == null) {
                    // Check if syntax is hostname:port or port is provided as separate argument
                    if (arg.contains(":")) {
                        String[] addressParts = arg.split(":");
                        if (addressParts.length == 2) {
                            hostname = addressParts[0];
                            try {
                                port = Integer.parseInt(addressParts[1]);
                            } catch (NumberFormatException x) {
                                usageAndExit();
                            }
                        }
                        else {
                            usageAndExit();
                        }
                    }
                    else {
                        // First non-option arg should be hostname
                        hostname = arg;
                    }
                }
                else {
                    // Second non-option arg should be port
                    try {
                        port = Integer.parseInt(arg);
                    } catch (NumberFormatException x) {
                        usageAndExit();
                    }
                }
            }
        }

        if (options.contains("-v") || options.contains("--version")) {
            System.out.println(VERSION);
            return;
        }

        if (options.contains("--clearPrefs")) {
            TopThreadsPanel.removePrefs();
            System.out.println("cleared saved preferences");
            return;
        }

        // By now, hostname and port should be set.
        if (hostname == null || port == null) {
            usageAndExit();
        }

        if (options.contains("--debug")) {
            debug = true;
        }

        if (options.contains("-t") || options.contains("--textui")) {
            new TextUI(hostname, port);
        }
        else {
            new SwingUI(hostname, port);
        }
    }

    static MBeanServerConnection connect(String hostname, int port) throws IOException {
        String connectSting = "/jndi/rmi://" + hostname + ":" + port + "/jmxrmi";
        MBeanServerConnection server = null;
        try {
            JMXServiceURL url = new JMXServiceURL("rmi", "", 0, connectSting);
            JMXConnector connector = JMXConnectorFactory.connect(url);
            return connector.getMBeanServerConnection();
        } catch (MalformedURLException e) {
            // impossible
            return null;  // just to satisfy compiler
        }
    }
}
