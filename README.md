
# TopThreads

TopThreads is a JConsole plugin to analyse CPU-usage per thread.

See <http://arnhem.luminis.eu/top-threads-plugin-for-jconsole/> and
<http://arnhem.luminis.eu/new_version_topthreads_jconsole_plugin/> for more info.

## Usage

To run TopThreads as JConsole plugin, pass the path to the jar file with the `-pluginpath` option:

    jconsole -pluginpath build/libs/topthreads.jar

TopThreads can also be run as stand alone (Swing) application, e.g.

    java -jar build/libs/topthreads.jar localhost 7007

or

    java -jar build/libs/topthreads.jar localhost:7007

Make sure the application you want to monitor is started with the correct jmx options; see <http://docs.oracle.com/javase/7/docs/technotes/guides/management/agent.html> for details. For example, if you don't need a secure JMX connection, adding the following system properties will do:

    -Dcom.sun.management.jmxremote.authenticate=false
    -Dcom.sun.management.jmxremote.ssl=false
    -Dcom.sun.management.jmxremote.port=<port>

TopThreads can also be run as console application, with limited functionality:

    java -jar build/libs/topthreads.jar --textui localhost:7007


## License

TopThreads is open source and licensed under LGPL (see the NOTICE.txt and LICENSE.txt files in the distribution). This means that you can use TopThreads for anything you like, and that you can embed it as a library in other applications, even commercial ones. If you do so, the author would appreciate if you include a reference to the original TopThreads application and website (one of the blogs mentioned above, or this readme). As of the LGPL license, all modifications and additions to the source code must be published as (L)GPL as well.

## Feedback

Any feedback is welcome. If you have questions or comments, post to one of the blogs mentioned above. If you discover a bug, please file an issue at <https://bitbucket.org/pjtr/topthreads/issues>.

